# General Examples (Dart)

## Functions

### Lambdas

> How to convert normal Functions to Lambda (Anonymous/Nameless) Functions

### Higher-order Functions

> How to code Higher-order Functions as normal Function to Lambda (Anonymous/Nameless) Functions

### Callbacks

> How to code Callbacks as normal Functions to Lambda (Anonymous/Nameless) Functions
>
> Using and not using async and await

### Closures

> How to code Closures
>
> Using and not using async and await
