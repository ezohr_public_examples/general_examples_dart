library functions_library;

import 'dart:async';

import 'package:meta/meta.dart';

// TODO: closures Unit Tests using the Test Cases Framework

// * Closures

Function returnedClosureWithoutAwaitLambda = ({@required String from}) => print(
      'returnedClosureWithoutAwaitLambda: I\'m the returnedClosureWithoutAwaitLambda function, originally called from $from',
    );

// Function closureWithoutAwaitLambda = ({@required String from}) {
//   return () {
//     returnedClosureWithoutAwaitLambda(from: from);
//   };
// };
Function closureWithoutAwaitLambda = ({@required String from}) => () => returnedClosureWithoutAwaitLambda(from: from);

Function runClosureWithoutAwaitLambda = ({@required String from}) => Future.delayed(
      Duration(seconds: 1),
      // The () is required due to the => () =>
      () => closureWithoutAwaitLambda(from: from)(),
    );

Function returnedClosureWithAwaitLambda = ({@required String from}) => 'returnedClosureWithAwaitLambda: I\'m the returnedClosureWithAwaitLambda function, originally called from $from';

// Function closureWithAwaitLambda = ({@required String from}) {
//   return () {
//     returnedClosureWithAwaitLambda(from: from);
//   };
// };
Function({@required String from}) closureWithAwaitLambda = ({@required String from}) => () => returnedClosureWithAwaitLambda(from: from);

// async/await in void main(List<String> arguments)
Function runClosureWithAwaitLambda = ({@required String from}) => Future.delayed(
      Duration(seconds: 1),
      // The () is required due to the => () =>
      () => closureWithAwaitLambda(from: from)(),
    );
