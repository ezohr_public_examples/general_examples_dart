library functions_library;

import 'dart:async';

import 'package:meta/meta.dart';

// TODO: callbacks Unit Tests using the Test Cases Framework

// * Callbacks: Long

void callbackWithoutAwaitLong({@required String from}) {
  print('callbackWithoutAwaitLong: I\'m the callbackWithoutAwaitLong function, called from $from');
}

void runCallbackWithoutAwaitLong({@required String from}) {
  Future.delayed(Duration(seconds: 1), () {
    return callbackWithoutAwaitLong(from: 'runCallbackWithoutAwaitLong');
  });
}

// async/await in void main(List<String> arguments)
Future<String> callbackWithAwaitLong({@required String from}) {
  return Future.value('callbackWithAwaitLong: I\'m the callbackWithAwaitLong function, called from $from');
}

// async/await in void main(List<String> arguments)
Future<String> runCallbackWithAwaitLong({@required String from}) {
  return Future.delayed(Duration(seconds: 1), () {
    return callbackWithAwaitLong(from: 'runCallbackWithAwaitLong');
  });
}

// * Callbacks: Short

void callbackWithoutAwaitShort({@required String from}) => print(
      'callbackWithoutAwaitShort: I\'m the callbackWithoutAwaitShort function, called from $from',
    );

void runCallbackWithoutAwaitShort({@required String from}) => Future.delayed(
      Duration(seconds: 1),
      () => callbackWithoutAwaitShort(from: 'runCallbackWithoutAwaitShort'),
    );

// async/await in void main(List<String> arguments)
Future<String> callbackWithAwaitShort({@required String from}) => Future.value(
      'callbackWithAwaitShort: I\'m the callbackWithAwaitShort function, called from $from',
    );

// async/await in void main(List<String> arguments)
Future<String> runCallbackWithAwaitShort({@required String from}) => Future.delayed(
      Duration(seconds: 1),
      () => callbackWithAwaitShort(from: 'runCallbackWithAwaitShort'),
    );

// * Callbacks: Lambda

Function callbackWithoutAwaitLambda = ({@required String from}) => print(
      'callbackWithoutAwaitLambda: I\'m the callbackWithoutAwaitLambda function, called from $from',
    );

Function runCallbackWithoutAwaitLambda = ({@required String from}) => Future.delayed(
      Duration(seconds: 1),
      () => callbackWithoutAwaitLambda(from: 'runCallbackWithoutAwaitLambda'),
    );

Function callbackWithAwaitLambda = ({@required String from}) => 'callbackWithAwaitLambda: I\'m the callbackWithAwaitLambda function, called from $from';

// async/await in void main(List<String> arguments)
Function runCallbackWithAwaitLambda = ({@required String from}) => Future.delayed(
      Duration(seconds: 1),
      () => callbackWithAwaitLambda(from: 'runCallbackWithAwaitLambda'),
    );
