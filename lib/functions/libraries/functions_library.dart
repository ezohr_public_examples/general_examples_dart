library functions_library;

import 'dart:async';

import 'package:meta/meta.dart';

// TODO: funtions Unit Tests using the Test Cases Framework

// * Lambdas

double subtractLong({@required double number0, @required double number1}) {
  return number0 - number1;
}

double subtractShort({@required double number0, @required double number1}) => number0 - number1;

Function subtractLambda = ({@required double number0, @required double number1}) => number0 - number1;

// * Higher-order Functions

Function sumLong({@required double number0, @required double number1}) {
  return (() {
    return number0 + number1;
  });
}

Function sumShort({@required double number0, @required double number1}) => () => number0 + number1;

Function sumLambda = ({@required double number0, @required double number1}) => () => number0 + number1;

double squareLong({@required Function function}) {
  return function() * function();
}

double squareShort({@required Function function}) => function() * function();

Function squareLambda = ({@required Function function}) => function() * function();
