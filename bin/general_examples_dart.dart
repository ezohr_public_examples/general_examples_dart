import 'package:general_examples_dart/callbacks/libraries/callbacks_library.dart';
import 'package:general_examples_dart/closures/libraries/closures_library.dart';
import 'package:general_examples_dart/functions/libraries/functions_library.dart';

void main(List<String> arguments) async {
  print('===');
  print('');

  print('Lambdas');

  final subtractLong0 = subtractLong(number0: 10.0, number1: 5.0);
  print('subtractLong: $subtractLong0');

  final subtractShort0 = subtractShort(number0: 10.0, number1: 5.0);
  print('subtractShort: $subtractShort0');

  final subtractLambda0 = subtractLambda(number0: 10.0, number1: 5.0);
  print('subtractLambda: $subtractLambda0');

  print('');
  print('===');
  print('');

  print('Higher-order Functions');

  final sumLong0 = sumLong(number0: 10.0, number1: 5.0);
  final squareLong0 = squareLong(function: sumLong0);
  print('squareLong: $squareLong0');

  final sumShort0 = sumShort(number0: 10.0, number1: 5.0);
  final squareShort0 = squareShort(function: sumShort0);
  print('squareShort: $squareShort0');

  final sumLambda0 = sumLambda(number0: 10.0, number1: 5.0);
  final squareLambda0 = squareLambda(function: sumLambda0);
  print('squareLambda: $squareLambda0');

  print('');
  print('===');
  print('');

  print('Callbacks');

  runCallbackWithoutAwaitLong(from: 'runCallbackWithoutAwaitLong1');
  print(await runCallbackWithAwaitLong(from: 'runCallbackWithAwaitLong1'));

  runCallbackWithoutAwaitShort(from: 'runCallbackWithoutAwaitShort1');
  print(await runCallbackWithAwaitShort(from: 'runCallbackWithAwaitShort1'));

  runCallbackWithoutAwaitLambda(from: 'runCallbackWithoutAwaitLambda');
  print(await runCallbackWithAwaitLambda(from: 'runCallbackWithAwaitLambda'));

  print('');
  print('===');
  print('');

  print('Closures');

  runClosureWithoutAwaitLambda(from: 'runClosureWithoutAwaitLambda');
  print(await runClosureWithAwaitLambda(from: 'runClosureWithAwaitLambda'));

  print('');
  print('===');
  print('');
}
